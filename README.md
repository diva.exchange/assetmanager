# DIVA Assets

## Important Notice

The binaries in the `build/bin` folder of this repository are checked (SHA256, pgp) against the official download repositories (bitcoin, monero, ethereum, etc.). However, nothing stops you from replacing these binaries with your own, locally trusted, versions. The folder name contains the version of the binary, like `build/bitcoin-22.0/bin/bitcoind` or `build/zcash-4.6.0/bin/zcashd`. 

## About

This is the "backend" of DIVA. The backend is able to perform actions on chains. A typical action is a transfer of an asset from account A to account B.

## How to Run Unit Tests

## Linting

To lint the code, use
```
npm run lint
```

## Contact the Developers

On [DIVA.EXCHANGE](https://www.diva.exchange) you'll find various options to get in touch with the team.

Talk to us via Telegram [https://t.me/diva_exchange_chat_de]() (English or German).

## Donations

Your donation goes entirely to the project. Your donation makes the development of DIVA.EXCHANGE faster.

XMR: 42QLvHvkc9bahHadQfEzuJJx4ZHnGhQzBXa8C9H3c472diEvVRzevwpN7VAUpCPePCiDhehH4BAWh8kYicoSxpusMmhfwgx

BTC: 3Ebuzhsbs6DrUQuwvMu722LhD8cNfhG1gs

Awesome, thank you!

## License

[AGPLv3](LICENSE)
