/**
 * Copyright (C) 2022 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import { Config } from './config';
import { Server } from './server';

class Main {
  private config: Config = {} as Config;

  static async make() {
    const self = new Main();
    self.config = new Config();
    await self.start();
  }

  private async start() {
    const server = new Server(this.config);
    process.once('SIGINT', async () => {
      await server.shutdown();
      process.exit(0);
    });
    process.once('SIGTERM', async () => {
      await server.shutdown();
      process.exit(0);
    });
    await server.start();
  }
}

(async () => {
  await Main.make();
})();
