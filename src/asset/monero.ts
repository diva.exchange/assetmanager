/**
 * Copyright (C) 2022 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import express, { Request, Response } from 'express';
import get from 'simple-get';
import { Config, FULLNODE, WALLET } from '../config';
import { Server } from '../server';
import { Logger } from '../logger';

export class Monero {
  private readonly rpc_ip: string;
  private readonly rpc_port: number;
  private readonly api_fullnode: string;

  static make(server: Server): Monero {
    const m = new Monero(server.config);

    switch (server.config.type) {
      case FULLNODE:
        m.initRoutesFullNode(server.app);
        break;
      case WALLET:
        m.initRoutesWallet(server.app);
        break;
    }

    // test for api availability
    (async () => {
      if (m.api_fullnode) {
        await m.api('/getinfo');
      }
    })();

    return m;
  }

  private constructor(c: Config) {
    this.rpc_ip = c.rpc_ip;
    this.rpc_port = c.rpc_port;
    this.api_fullnode = c.api_fullnode;
  }

  //@FIXME access with token only (paywall)
  /**
   * Requires a token to be accessed
   * @param app
   */
  private initRoutesFullNode(app: express.Express) {
    app.get('/getinfo', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'getinfo',
        },
        res
      );
    });
  }

  private initRoutesWallet(app: express.Express) {
    app.get('/create_wallet/:name', (req: Request, res: Response) => {
      const filename = (req.params['name'] || '').toLowerCase().replace(/[^a-z0-9_]/g, '');
      if (!filename) {
        return res.status(400).end();
      }
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'create_wallet',
          params: { filename: filename, language: 'English' }
        },
        res
      );
    });

    app.get('/open_wallet/:name', (req: Request, res: Response) => {
      const filename = (req.params['name'] || '').toLowerCase().replace(/[^a-z0-9_]/g, '');
      if (!filename) {
        return res.status(400).end();
      }
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'open_wallet',
          params: { filename: filename }
        },
        res
      );
    });

    app.get('/close_wallet', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'close_wallet',
        },
        res
      );
    });

    app.get('/get_height', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'get_height',
        },
        res
      );
    });

    app.get('/get_accounts', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'get_accounts',
        },
        res
      );
    });

    app.get('/get_transfers/in', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'get_transfers',
          params: { in: true },
        },
        res
      );
    });

    app.get('/get_transfers/out', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'get_transfers',
          params: { out: true },
        },
        res
      );
    });

    app.get('/get_transfers/pending', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'get_transfers',
          params: { pending: true },
        },
        res
      );
    });

    app.get('/get_transfers/failed', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '2.0',
          method: 'get_transfers',
          params: { failed: true },
        },
        res
      );
    });
  }

  private processJSONRPC(json: object, res: Response) {
    const options = {
      method: 'POST',
      url: `http://${this.rpc_ip}:${this.rpc_port}/json_rpc`,
      timeout: 10000,
      followRedirects: false,
      body: json,
      json:true,
    };

    get.concat(options, (error: Error, resPost: Response, data: Buffer) => {
      if (error || resPost.statusCode !== 200) {
        //@FIXME logging
        error && Logger.trace(error.toString());
        resPost && resPost.statusCode ? res.status(resPost.statusCode || 500).end() : res.status(500).end();
      } else {
        res.json(data);
      }
    });
  }

  private async rpc(json: object): Promise<any> {
    const options = {
      method: 'POST',
      url: `http://${this.rpc_ip}:${this.rpc_port}/json_rpc`,
      timeout: 10000,
      followRedirects: false,
      body: JSON.stringify(json),
      headers: { 'content-type': 'application/json' },
    };

    return new Promise((resolve, reject) => {
      get.concat(options, (error: Error, resPost: Response, data: Buffer) => {
        if (error || resPost.statusCode !== 200) {
          reject({ statusCode: resPost.statusCode, error: error });
        } else {
          try {
            resolve(JSON.parse(data.toString()));
          } catch (error: any) {
            //@FIXME logging
            Logger.trace(error.toString());
            reject({ statusCode: 500, error: error });
          }
        }
      });
    });
  }

  private async api(endpoint: string, timeout: number = 1000): Promise<string> {
    const options = {
      method: 'GET',
      url: this.api_fullnode + endpoint,
      timeout: timeout,
      followRedirects: false,
      headers: { 'diva-token': 'some-token' },
    };

    return new Promise((resolve, reject) => {
      get.concat(options, (error: Error, res: Response, data: Buffer) => {
        if (error || res.statusCode !== 200) {
          reject(error || new Error(`${res.statusCode || 400}`));
        } else {
          resolve(data.toString());
        }
      });
    });
  }
}
