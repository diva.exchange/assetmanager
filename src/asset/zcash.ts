/**
 * Copyright (C) 2022 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import express, { Request, Response } from 'express';
import get from 'simple-get';
import { Config, FULLNODE, WALLET } from '../config';
import { Server } from '../server';
import { Logger } from '../logger';

export class Zcash {
  private readonly rpc_ip: string;
  private readonly rpc_port: number;
  private readonly rpc_user: string;
  private readonly rpc_password: string;
  private readonly api_fullnode: string;

  static make(server: Server): Zcash {
    const z = new Zcash(server.config);

    switch (server.config.type) {
      case FULLNODE:
        z.initRoutesFullNode(server.app);
        break;
      case WALLET:
        z.initRoutesWallet(server.app);
        break;
    }

    // test for api availability
    (async () => {
      if (z.api_fullnode) {
        await z.api('/getinfo');
      }
    })();

    return z;
  }

  private constructor(c: Config) {
    this.rpc_ip = c.rpc_ip;
    this.rpc_port = c.rpc_port;
    this.rpc_user = c.rpc_user;
    this.rpc_password = c.rpc_password;
    this.api_fullnode = c.api_fullnode;
  }

  //@FIXME access with token only (paywall)
  /**
   * Requires a token to be accessed
   * @param app
   */
  private initRoutesFullNode(app: express.Express) {
    app.get('/getinfo', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'getinfo',
        },
        res
      );
    });

    app.get('/getwalletinfo', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'getwalletinfo',
        },
        res
      );
    });

    app.get('/importaddress/:address/', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'importaddress',
          params: [req.params['address'] || '', '', false],
        },
        res
      );
    });

    app.get('/importpubkey/:pubkey', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'importpubkey',
          params: [req.params['pubkey'] || '', '', false],
        },
        res
      );
    });

    app.get('/listaddresses', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'listaddresses',
        },
        res
      );
    });

    app.get('/validateaddress/:address', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'validateaddress',
          params: [req.params['address'] || ''],
        },
        res
      );
    });

    app.get('/listunspent/:address', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'listunspent',
          params: [1, 9999999, [req.params['address'] || '']],
        },
        res
      );
    });

    app.get('/getreceivedbyaddress/:address/:minconf?/:inZat?', (req: Request, res: Response) => {
      const minconf = Math.floor(Number(req.params['minconf'] || 1));
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'getreceivedbyaddress',
          params: [req.params['address'] || '', minconf > 1 ? minconf : 1, Number(req.params['inZat'] || 0) > 0],
        },
        res
      );
    });

    app.get('/sendrawtransaction/:hexstring/:allowhighfees?', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'sendrawtransaction',
          params: [req.params['hexstring'] || '', Number(req.params['allowhighfees'] || 0) > 0],
        },
        res
      );
    });
  }

  private initRoutesWallet(app: express.Express) {
    app.get('/getwalletinfo', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'getwalletinfo',
        },
        res
      );
    });

    /** fullnode: ztestsapling1zk9fq5nx9ppf2k845lk4ks8cz5jcn35u236vle9atygygleqp75ly4mjksgh4t0wpue6zxpppjw */
    /** wallet:   ztestsapling153t38g5zw0gjt0fxhdch8md84mp8962zmgdnt3xs2lmaqttq8f9evw78kzxsh9d4ylmyxek5c22 */
    /** wallet:   ztestsapling1asl4y58tgzmzrda0yrqju67zjyhjzu88n0ggsfy53t9tvgm9vpejxl3pwp3mdtf27tk3q4zgnpu */
    app.get('/getnewaddress', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'getnewaddress',
        },
        res
      );
    });

    app.get('/validateaddress/:address', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'validateaddress',
          params: [req.params['address'] || ''],
        },
        res
      );
    });

    app.get('/listaddresses', (req: Request, res: Response) => {
      this.processJSONRPC(
        {
          jsonrpc: '1.0',
          method: 'listaddresses',
        },
        res
      );
    });

    app.get('/transfer/:fromaddress/:toaddress/:amount', async (req: Request, res: Response) => {
      const from = req.params['fromaddress'] || '';
      const to = req.params['toaddress'] || '';
      const amount = Number(req.params['amount'] || 0);
      if (!from || !to || amount <= 0) {
        return res.status(400).end();
      }

      let sum = 0;
      const objResult = JSON.parse(await this.api('/listunspent/' + from));
      let arrayInput: Array<{ txid: string; vout: number }> = [];
      // get unspent transactions
      objResult.result.forEach((tx: any) => {
        if (sum < amount) {
          arrayInput.push({ txid: tx.txid, vout: tx.outindex });
          sum = sum + tx.amount;
        }
      });
      Logger.trace(arrayInput);
      const json = {
          jsonrpc: '1.0',
          method: 'createrawtransaction',
          params: [arrayInput, { [to]: amount }],
        };
      Logger.trace(JSON.stringify(json));

      let objResponse = {};
      try {
        objResponse = await this.rpc(json);
        //@FIXME logging
        Logger.trace(objResponse);
      } catch (error) {
        //@FIXME logging
        Logger.trace(error);
        return res.status(400).end();
      }

      return res.status(200).end();
    });
  }

  private processJSONRPC(json: object, res: Response) {
    const options = {
      method: 'POST',
      url: `http://${this.rpc_user}:${this.rpc_password}@${this.rpc_ip}:${this.rpc_port}`,
      timeout: 60000,
      followRedirects: false,
      body: JSON.stringify(json),
      headers: { 'content-type': 'text/plain' },
    };

    get.concat(options, (error: Error, resPost: Response, data: Buffer) => {
      if (error || resPost.statusCode !== 200) {
        //@FIXME logging
        error && Logger.trace(error.toString());
        resPost && resPost.statusCode ? res.status(resPost.statusCode || 500).end() : res.status(500).end();
      } else {
        res.json(JSON.parse(data.toString()));
      }
    });
  }

  private async rpc(json: object): Promise<any> {
    const options = {
      method: 'POST',
      url: `http://${this.rpc_user}:${this.rpc_password}@${this.rpc_ip}:${this.rpc_port}`,
      timeout: 1000,
      followRedirects: false,
      body: JSON.stringify(json),
      headers: { 'content-type': 'text/plain' },
    };

    return new Promise((resolve, reject) => {
      get.concat(options, (error: Error, resPost: Response, data: Buffer) => {
        if (error || resPost.statusCode !== 200) {
          reject({ statusCode: resPost.statusCode, error: error });
        } else {
          try {
            resolve(JSON.parse(data.toString()));
          } catch (error: any) {
            //@FIXME logging
            Logger.trace(error.toString());
            reject({ statusCode: 500, error: error });
          }
        }
      });
    });
  }

  private async api(endpoint: string, timeout: number = 1000): Promise<string> {
    const options = {
      method: 'GET',
      url: this.api_fullnode + endpoint,
      timeout: timeout,
      followRedirects: false,
      headers: { 'diva-token': 'some-token' },
    };

    return new Promise((resolve, reject) => {
      get.concat(options, (error: Error, res: Response, data: Buffer) => {
        if (error || res.statusCode !== 200) {
          reject(error || new Error(`${res.statusCode || 400}`));
        } else {
          resolve(data.toString());
        }
      });
    });
  }
}
