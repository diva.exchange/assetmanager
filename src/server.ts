/**
 * Copyright (C) 2022 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import { Config, MONERO, BITCOIN, ETHEREUM, ZCASH } from './config';
import { Logger } from './logger';
import createError from 'http-errors';
import express, { Express, NextFunction, Request, Response } from 'express';
import http from 'http';
import compression from 'compression';
import path from 'path';
import { Zcash } from './asset/zcash';
import { Monero } from './asset/monero';

export class Server {
  public readonly config: Config;
  public readonly app: Express;

  private readonly httpServer: http.Server;

  constructor(config: Config) {
    this.config = config;
    Logger.info(`diva assetmanager ${this.config.VERSION} instantiating...`);

    // express application
    this.app = express();
    // hide express
    this.app.set('x-powered-by', false);

    // compression
    this.app.use(compression());

    // static content
    this.app.use(express.static(path.join(config.path_app, 'static')));

    // json
    this.app.use(express.json());

    // routes
    this.initRoutes();

    // catch 404 and forward to error handler
    this.app.use((req: Request, res: Response, next: NextFunction) => {
      next(createError(404));
    });

    // error handler
    this.app.use(Server.error);

    // Web Server
    this.httpServer = http.createServer(this.app);
    this.httpServer.on('listening', () => {
      Logger.info(`HttpServer listening on ${this.config.ip}:${this.config.port}`);
    });
    this.httpServer.on('close', () => {
      Logger.info(`HttpServer closing on ${this.config.ip}:${this.config.port}`);
    });
  }

  async start(): Promise<Server> {
    await this.httpServer.listen(this.config.port, this.config.ip);
    return this;
  }

  async shutdown(): Promise<void> {
    if (this.httpServer) {
      return await new Promise((resolve) => {
        this.httpServer.close(() => {
          resolve();
        });
      });
    }
    return Promise.resolve();
  }

  private initRoutes() {
    // catch unavailable favicon.ico
    this.app.get('/favicon.ico', (req: Request, res: Response) => {
      return res.sendStatus(204);
    });

    this.app.get('/about', (req: Request, res: Response) => {
      return res.json({
        version: this.config.VERSION,
      });
    });

    // load asset api's
    switch (this.config.asset) {
      case MONERO:
        Monero.make(this);
        break;
      case ZCASH:
        Zcash.make(this);
        break;
      case BITCOIN:
      case ETHEREUM:
        //@TODO
        throw new Error('not implemented');
    }
  }

  private static error(err: any, req: Request, res: Response, next: NextFunction) {
    res.status(err.status || 500);

    res.json({
      path: req.path,
      status: err.status || 500,
      message: err.message,
      error: process.env.NODE_ENV === 'development' ? err : {},
    });

    next();
  }
}
