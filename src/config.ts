/**
 * Copyright (C) 2022 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import fs from 'fs';
import path from 'path';

export const MONERO = 'monero';
export const BITCOIN = 'bitcoin';
export const ETHEREUM = 'ethereum';
export const ZCASH = 'zcash';

export const FULLNODE = 'fullnode';
export const WALLET = 'wallet';

export const MAINNET = 'mainnet';
export const TESTNET = 'testnet';

const DEFAULT_IP = '127.0.0.1';
const DEFAULT_PORT = 17460;

export type Configuration = {
  path_app?: string;

  asset?: string;
  type?: string;
  network?: string;

  ip?: string;
  port?: number;

  rpc_ip?: string;
  rpc_port?: number;
  rpc_user?: string;
  rpc_password?: string;

  api_fullnode?: string;
};

export class Config {
  public readonly path_app: string;
  public readonly VERSION: string;

  public readonly asset: string;
  public readonly type: string;
  public readonly network: string;

  public readonly ip: string;
  public readonly port: number;

  public readonly rpc_ip: string;
  public readonly rpc_port: number;
  public readonly rpc_user: string;
  public readonly rpc_password: string;

  public readonly api_fullnode: string;

  constructor() {
    let c: Configuration = {} as Configuration;

    // setting the path, if the executable is a packaged binary (see "pkg --help")
    if (Object.keys(process).includes('pkg')) {
      this.path_app = path.dirname(process.execPath);
    } else {
      this.path_app = path.join(__dirname, '/../');
    }

    // read config file
    if (fs.existsSync(path.join(this.path_app, 'config.json'))) {
      c = require(path.join(this.path_app, 'config.json'));
    }

    this.VERSION = fs.readFileSync(path.join(this.path_app, 'static', 'version.txt')).toString();

    this.asset = (process.env.ASSET || c.asset) as string;
    switch (this.asset) {
      case MONERO:
      case BITCOIN:
      case ETHEREUM:
      case ZCASH:
        break;
      default:
        throw new Error('Asset not supported');
    }

    this.type = (process.env.TYPE || c.type) as string;
    switch (process.env.TYPE || c.type) {
      case FULLNODE:
      case WALLET:
        break;
      default:
        this.type = WALLET;
    }

    this.network = (process.env.NETWORK || c.network) as string;
    switch (process.env.NETWORK || c.network) {
      case MAINNET:
      case TESTNET:
        break;
      default:
        this.network = TESTNET;
    }

    this.ip = process.env.IP || c.ip || DEFAULT_IP;
    this.port = Config.port(process.env.PORT || c.port || DEFAULT_PORT);

    this.rpc_ip = process.env.RPC_IP || c.rpc_ip || '';
    this.rpc_port = Config.port(process.env.RPC_PORT || c.rpc_port || 0);
    this.rpc_user = process.env.RPC_USER || c.rpc_user || '';
    this.rpc_password = process.env.RPC_PASSWORD || c.rpc_password || '';

    this.api_fullnode = process.env.API_FULLNODE || c.api_fullnode || '';
  }

  private static tf(n: any): boolean {
    return Number(n) > 0;
  }

  private static b(n: any, min: number, max: number): number {
    n = Number(n);
    min = Math.floor(min);
    max = Math.ceil(max);
    return n >= min && n <= max ? Math.floor(n) : n > max ? max : min;
  }

  private static port(n: any): number {
    return Number(n) ? Config.b(Number(n), 1025, 65535) : 0;
  }
}
