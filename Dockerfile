#
# Copyright (C) 2022 diva.exchange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
#

FROM node:14-slim AS build

LABEL author="Konrad Baechler <konrad@diva.exchange>" \
  maintainer="Konrad Baechler <konrad@diva.exchange>" \
  name="assetmanager" \
  description="Distributed digital value exchange upholding security, reliability and privacy" \
  url="https://diva.exchange"

#############################################
# First stage: container used to build the binary
#############################################
COPY bin/build.sh /assetmanager/bin/build.sh
COPY bin/util/* /assetmanager/bin/util/
COPY src /assetmanager/src
COPY package.json /assetmanager/package.json
COPY tsconfig.json /assetmanager/tsconfig.json

RUN cd assetmanager \
  && mkdir build \
  && mkdir dist \
  && npm i -g pkg@5.5.1 \
  && npm i --production \
  && bin/build.sh

#############################################
# Second stage: create the distroless image
#############################################
FROM gcr.io/distroless/cc
COPY package.json /package.json
COPY static /static

# Copy the binary and the prebuilt dependencies
COPY --from=build /assetmanager/build/assetmanager-linux-x64 /assetmanager

ENTRYPOINT [ "/assetmanager" ]
